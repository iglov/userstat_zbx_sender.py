#!/usr/bin/python3

#                                                #
# $Id: Wed Apr 18 16:01:53 EEST 2018 iglov Exp $ #
#                                                #

#
# Sources: https://gitlab.com/iglov/userstat_zbx_sender.py
#

import os
import sys
import fcntl
import json
import argparse
import tempfile
import subprocess
import mysql.connector
from mysql.connector import Error
from configparser import ConfigParser

class ConnectionError(Exception):
    pass

class CredentialsError(Exception):
    pass

class SQLError(Exception):
    pass

class Database:
    """In this class realized control of connect by mysql.connector with MySQL.
    When some troubles with connection, your session (cursor and connection socket) will be closed.
    You must use this class by python operator 'with'.
    """
    def __init__(self, config: dict):
        self.configuration = config

    def __enter__(self) -> 'cursor':
        try:
            self.conn = mysql.connector.connect(**self.configuration)
            self.cursor = self.conn.cursor()
            return self.cursor
        except mysql.connector.errors.InterfaceError as err:
            raise ConnectionError(err)
        except mysql.connector.errors.ProgrammingError as err:
            raise CredentialsError(err)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.conn.rollback()
        self.cursor.close()
        self.conn.close()

        if exc_type is mysql.connector.errors.ProgrammingError:
            raise SQLError(exc_value)
        elif exc_type:
            raise exc_type(exc_value)

def parse_args():
    """Initiate an argparse, define arguments, and return a Namespace of args"""
    parser = argparse.ArgumentParser(description='MySQL USERSTAT metrics discovery and send utility')
    parser.add_argument('-d', '--discovery', action='store_true', default=False, 
                        help="Get from MySQL tables and send it to zabbix as LLD")
    parser.add_argument('-m', '--metrics', action='store_true', default=False,
                        help="Get from MySQL metrics and send it to zabbix")
    parser.add_argument('-t', '--top', action='store_true', default=False,
                        help="Get from MySQL top 10 metrics and send it to zabbix")

    return parser.parse_args()

def lock(filename):
    """Get a word(str) then put it on filename (ex.: /tmp/my.script.py.HERE_THIS_WORD.lock)
    and lock it by fcntl module.
    """
    lock_file = open(os.path.join(tempfile.gettempdir(), '{0}.{1}.lock'.format(os.path.basename(__file__), filename)), 'w')

    try:
        fcntl.lockf(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        print("My another instance already running. Do nothing")
        sys.exit(0)

def get_conf(filename='userstat_zbx_sender.ini', section='mysql'):
    """Get filename as str with name of file with parameters, 
    and section on this file as str.
    Then parse this by ConfigParser module and return a dictionary.
    """
    parser = ConfigParser()
    parser.read(filename)
    conf = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items: conf[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))
    return conf

def sendZabbix(input_file, conf: dict):
    """Get input_file as str (ex.: /tmp/temp2na5hnSAw12) and conf as dictionary.
    From conf get some parameters and with subprocess send input_file by zabbix_sender
    """
    sender = '/usr/bin/zabbix_sender'
    if not os.path.isfile(sender):
        print('Zabbix sender is not found. You must install packet zabbix-sender first')
        return None

    command = [sender]
    command.append('--zabbix-server={0}'.format(conf['server']))
    command.append('--port={0}'.format(conf['port']))
    command.append('--input-file={0}'.format(input_file))

    P = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    P.wait()

if __name__ == '__main__':
    data       = []
    tables_lld = {"data":[]}
    args       = parse_args()

    if args.discovery:
        lock("discovery")
    elif args.metrics:
        lock("metrics")
    elif args.top:
        lock("top")
    else:
        print ("You must set the key. Use -h for help")
        sys.exit(0)

    with Database(get_conf()) as cursor:
        # Use hint of maxscale for route request to master
        _SQL = """select TABLE_NAME, ROWS_CHANGED, ROWS_READ from information_schema.TABLE_STATISTICS where TABLE_SCHEMA = 'some_table' -- maxscale route to master"""
        cursor.execute(_SQL)
        data = cursor.fetchall()

    # In python3 we must convert our string to utf-8 first, or open file as utf-8 encoded
    with tempfile.NamedTemporaryFile('w+t', encoding='utf-8') as fp:
        fp.seek(0)

        if args.discovery or args.metrics:
            for (table_name, rows_changed, rows_read) in data:
                tables_lld['data'].append({'{#TABLE}': table_name})
                if args.metrics:
                    print("zabbix.virt4 rows_changed[{0}] {1}".format(table_name, rows_changed), file=fp)
                    print("zabbix.virt4 rows_read[{0}] {1}".format(table_name, rows_read), file=fp)

            if args.discovery:
                print("zabbix.virt4 ss.tables.discovery {0}".format(json.dumps(tables_lld)), file=fp)

        elif args.top:
            # ex. data = [('path', 44512140, 4352), ('sys_cur_calls', 8141503, 53522), ...]
            # Sort list of tuples by second element from highest to lowest
            # and put it to sorted_data as one string delimited by ','
            sorted_rows_changed = ','.join([x for x,_,_ in sorted(data, key=lambda tup: tup[1], reverse=True)][:10])
            print("zabbix.virt4 ss.tables.top_changed {0}".format(sorted_rows_changed), file=fp)
            sorted_rows_read = ','.join([x for x,_,_ in sorted(data, key=lambda tup: tup[2], reverse=True)][:10])
            print("zabbix.virt4 ss.tables.top_read {0}".format(sorted_rows_read), file=fp)

        fp.flush()
        fp.seek(0)

        sendZabbix(fp.name, get_conf(section='zabbix'))
